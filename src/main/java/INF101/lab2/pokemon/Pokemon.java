package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {

    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random = new Random();

    public Pokemon(String name) {
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        if (healthPoints > 0) {
            return true;
        } 
        else {
            return false;
        }
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        System.out.println(this.name + " attacks " + target.getName()+".");
        target.damage(damageInflicted); 
        if (target.getCurrentHP() == 0) {
            System.out.println(target.getName() + " is defeated by "+this.name+".");
        }
    }

    @Override
    public void damage(int damageTaken) {
        if (damageTaken < 0) {
            damageTaken = 0;
        }
        healthPoints -= damageTaken;
        if (healthPoints < 0) {
            healthPoints = 0;
        }
        System.out.println(this.name+" takes "+damageTaken+" damage and is left with "+healthPoints+"/"+maxHealthPoints+" HP");
    }

    @Override
    public String toString() {
        return this.name + " HP:" + " (" + healthPoints + "/" + maxHealthPoints + ") STR: " + strength;
    }

}
